Es gibt mehrere Möglichkeiten wie du etwas Beitragen kannst.

# Ohne Git
Wenn du von Git und GitLab keine Ahnung hast, kannst du dennoch etwas beitragen. Du kannst alle Aufgaben erledigen, welche als Text editiert werden können. Dazu muss in der TODO Liste stehen, dass diese Aufgabe auch ohne Git durchgeführt werden kann. Beachte bitte, dass du für jede Aufgabe, welche du ohne Git löst um EINEN Punkt weniger erhältst, als wenn du direkt mit Git arbeitest. Das hängt damit zusammen, dass andere deine Aufgaben einpflegen müssen.

1. Erstell eine Liste an Änderungen/ Korrekturen/ Verbesserungen, eine .tex Datei oder ein pdf von einem eingescannten Dokument.
1. Wenn möglich führe einen test Build durch um zu sehen, ob dein LaTeX Code auch wirklich funktioniert.
    
    ```
    $ make test
    ```
    Im /conf/out Ordner wird ein test.pdf erstellt.

## Über ein Issue
Erstelle ein Issue und kopiere dort die Datei hinein.

## Telegram Gruppe vom OMN
Schicke deine Lösung an die [Telegram Gruppe vom OMN](https://t.me/joinchat/FGFedBNMoUkUMscg8a0fgQ).

# Mit Git und GitLab
Wenn du dich mit Git, GitLab und LaTeX auskennst, wähle bitte diesen Weg.

## Einmalige Initialisierung
Hier wird beschrieben, welche Schritte einmalig durchführen musst, um das Repo auf deinen PC zu bekommen.

1. Registriere und melde dich bei GitLab an.
1. Lade dir [Git](https://git-scm.com/download/win) herunter.
1. Installiere Git, aktiviere im Installationsdialog "Git Credential Manager" und "Enable Symbolic Links"
1. Lade dir einen LaTeX Editor herunter. Empfohlen für Windows nutzer, [TeXStudio](https://github.com/texstudio-org/texstudio/releases/download/2.12.14/texstudio-2.12.14-win-qt5.exe)
1. Geh in GitLab auf die Seite vom [OMN-Dev-Team](https://gitlab.com/omn-dev-team). Such deine Lehrveranstaltung, zu der du etwas beitragen möchtest und clicke auf den Link. Diese Git Repo heißt für jede Lehrveranstaltung UPSTREAM
1. Klick rechts oben auf das Feld "Fork" um einen Abkömmling zu erstellen. GitLab erstellt dir eine Kopie vom UPSTREAM Repo. Deine Kopie heißt jetzt ORIGIN.
1. Klick in deinem soeben erstellten Abkömmling auf das Feld "Clone".
1. Das ist DEINE ORIGIN_URL, kopiere Sie.
1. Öffne deinen Explorer und erstelle einen Ordner für Uni.
1. Öffne diesen Ordner und rechtsklicke und klick auf Git Bash here.
1. Kopiere (Clone) deinen Fork auf deinen lokalen PC. Gib dazu in die Konsole folgende Befehle ohne dem $ Zeichen ein.  Die URL musst du durch deine kopierte ORIGIN_URL ersetzen.

    ```
    $ git clone ORIGIN_URL
    $ cd ./Type-LVA_Nummer-Name-der-LVA
    $ git submodule init conf
    $ git submodule update conf
    ```
1. Git lädt das open und conf Repo auf deinen PC herunter.
1. Wir fügen jetzt in den nächsten Schritten das UPSTREAM Projekt als Link hinzu. Damit kannst du dir neue Inhalte vom OMN holen.
1. Geh dazu in GitLab auf die UPSTREAM Lehrveranstaltung, zu der du etwas beitragen möchtest.
1. Klicke auf das Feld "Clone".
1. Das ist die UPSTREAM_URL, kopiere Sie.
1. Geh wieder in die Konsole und füge das UPSTREAM Repo mit folgendem Befehl hinzu.

    ```
    $ git remote add -t master upstream UPSTREAM_URL
    ```
1. Überprüfe deine remote Repos. Gib dazu in der Konsole folgende Befehle ein. Es sollten hier 4 Zeilen erscheinen

    ```
    $ git remote -v
    origin   ORIGIN_URL (fetch)
    origin   ORIGIN_URL (push)
    upstream UPSTREAM_URL (fetch)
    upstream UPSTREAM_URL (push)
    ```
    
## Updates vom UPSTREAM Projekt holen
Hier wird beschrieben, wie du neue Inhalte vom UPSTREAM Projekt erhältst.

1. Überprüfe ob du lokale, nicht commitete, Datein hast.

    ```
    $ git status
    ```
1. Sieht deine Ausgabe wie folgt aus, kannst du fortfahren. Steht hier etwas anderes, führe den Schritt "Commit erstellen" durch.

	```
	Auf Branch master
	nichts zu committen, Arbeitsverzeichnis unverändert
	```
1. Hole dir die neuesten Inhalte vom UPSTREAM Repo.

    ```
    $ git pull upstream master
    ```

## Inhalte erstellen
Hier wird beschrieben, welche Schritte notwendig sind damit du Inhalte erstellen kannst. Führe diese Schritte jedes mal in dieser Reinfolge aus.

1. Führe den Schritt "Updates vom UPSTREAM Projekt holen" durch, damit du am aktuellsten Stand bist.
1. Führe deine Änderungen durch. z.B. (neue LaTeX Dokumente, neue Source Code, neue PDFs, usw.)
1. Wenn du LaTeX Datein verändert hast führe alle 30 Mintuten einen test Build durch um zu sehen, ob dein Code auch wirklich funktioniert. Im /conf/out/ Ordner wird ein test.pdf mit Datein, welche in den letzten 60 Minuten bearbeitet wurden, erstellt. Führe dazu im Hauptordner folgenden Befehl aus.
    
    ```
    $ make test
    ```
1. Führe den Schritt "Commit erstellen" durch.

## Commit erstellen
Hier wird beschrieben, wie du einen Commit erstellen kannst.

1. Schau dir deine Änderungen mit

    ```
    $ git status
    ```
    an.
    Das kann dann so aussehen:

    ```
    Auf Branch master
	Änderungen, die nicht zum Commit vorgemerkt sind:
	(benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)
	(benutzen Sie "git checkout -- <Datei>...", um die Änderungen im Arbeitsverzeichnis zu verwerfen)
	(committen oder verwerfen Sie den unversionierten oder geänderten Inhalt in den Submodulen)

        geändert:       .gitignore
		geändert:       cld (geänderter Inhalt, unversionierter Inhalt)
        geändert:       conf (neue Commits, geänderter Inhalt, unversionierter Inhalt)
        geändert:       opn/conf/def.tex
        geändert:       opn/conf/name
        geändert:       opn/conf/nr
        geändert:       opn/conf/type

	Unversionierte Dateien:
	(benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)

        opn/conf/exm-mdl.tex
        opn/conf/qr-code-gitlab.png
        opn/conf/qr-code-telegram.png

	keine Änderungen zum Commit vorgemerkt (benutzen Sie "git add" und/oder "git commit -a")
    ```
1. Füge eine oder mehrere Datein, welche du hinzufügen möchtest mit folgendem Befehl hinzu. Füge niemals conf oder cld zu deinem Commit hinzu. Solltest du deshalb Probleme haben wende dich an die [Telegram Gruppe vom OMN](https://t.me/joinchat/FGFedBNMoUkUMscg8a0fgQ). 

    ```
    $ git add opn/conf/def.tex
    ```
    
    Schau dir mit diesem Befehl an, ob git die Datei auch akzeptiert hat. Git hat die Datei akzeptiert, da sie unter dem Text "zum Commit vorgemerkte Änderungen:" steht.
    
    ```
    $ git status
    Auf Branch master
	zum Commit vorgemerkte Änderungen:
	(benutzen Sie "git reset HEAD <Datei>..." zum Entfernen aus der Staging-Area)

        geändert:       opn/conf/def.tex

	Änderungen, die nicht zum Commit vorgemerkt sind:
	(benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)
	(benutzen Sie "git checkout -- <Datei>...", um die Änderungen im Arbeitsverzeichnis zu verwerfen)
	(committen oder verwerfen Sie den unversionierten oder geänderten Inhalt in den Submodulen)

        geändert:       .gitignore
		geändert:       cld (geänderter Inhalt, unversionierter Inhalt)
        geändert:       conf (neue Commits, geänderter Inhalt, unversionierter Inhalt)
        geändert:       opn/conf/name
        geändert:       opn/conf/nr
        geändert:       opn/conf/type

	Unversionierte Dateien:
	(benutzen Sie "git add <Datei>...", um die Änderungen zum Commit vorzumerken)

        opn/conf/exm-mdl.tex
        opn/conf/qr-code-gitlab.png
        opn/conf/qr-code-telegram.png
    ```
1. Schreibe einen aussagekräftigen Kommentar zu deinen Änderungen mit:

    ```
    $ git commit -m"Dein aussagekräftiger Kommentar zu deinen Änderungen."
    [master 1234567] Dein aussagekräftiger Kommentar zu deinen Änderungen.
	1 files changed, 1 insertions(+), 1 deletions(-)
    ```

## Commits Pushen
Hier wird beschrieben wie du deine Commits (können auch mehrere sein) an deinen GitLab Server schickst und einen Merge Request erstellst.

1. Führe den Punkt "Update vom UPSTREAM Projekt holen" aus, damit du am aktuellen stand bist.
1. Git folgenden Befehl in die Konsole ein, um das Logfile zu sehen. Die zwei obersten Commits sind von dir (Author: Dein_Name) am Mon 1.Jan.2019 um 14 und 13 Uhr erstellt worden. Der dritte Commit von oben ist von deinem Kollegen um 11Uhr erstellt worden. Er befindet sich schon auf dem UPSTREAM Server. Das ist ersichtlich an dem Hinweis nach der commit ID (upstream/master). Davor hast du um 10Uhr einen Commit erstellt. Dieser Commit befindet sich auf deinem GitLab Server (Hinweis mit origin/master). Alle neueren Commits befinden sich noch nicht auf deinem GitLab Server.
	
	```
	$ git log
	commit 1234567890123456789012345678901234567890 (HEAD -> master)
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 14:00:00 2019 +0100

		Dein 3. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 13:00:00 2019 +0100

		Dein 2. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890 (upstream/master)
	Author: Name_Kollege <1234567-Name_Kollege@users.noreply.gitlab.com>
	Date:   Mon Jan 1 11:00:00 2019 +0100

		Aussagekräftiger Kommentar von deinem Kollegen zu seinen Änderungen.
		
	commit 1234567890123456789012345678901234567890 (origin/master)
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 10:00:00 2019 +0100

		Dein 1. aussagekräftiger Kommentar zu deinen Änderungen.
		
	```
	
1. Push deine Commits auf deinen GitLab Server (Fork), also ORIGIN mit:

    ```
    $ git push origin master
    ```
    Sie dir erneut das log an. Es sieht nun so aus. Im obersten Commit ist vermerkt, dass sich dein GitLab Server ebenfalls auf dem selben Stand befindet wie dein lokalen Rechner.
    
    ```
    $ git log
	commit 1234567890123456789012345678901234567890 (HEAD -> master,origin/master)
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 14:00:00 2019 +0100

		Dein 3. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 13:00:00 2019 +0100

		Dein 2. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890 (upstream/master)
	Author: Name_Kollege <1234567-Name_Kollege@users.noreply.gitlab.com>
	Date:   Mon Jan 1 11:00:00 2019 +0100

		Aussagekräftiger Kommentar von deinem Kollegen zu seinen Änderungen.
		
	commit 1234567890123456789012345678901234567890 
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 10:00:00 2019 +0100

		Dein 1. aussagekräftiger Kommentar zu deinen Änderungen.
		
	```
	
1. Geh auf die GitLab Seite der UPSTREAM Lehrveranstaltung und erstelle einen Merge-Request.
1. Nachdem dein Merge-Request überprüft wurde, werden deine Commits in das UPSTREAM Projekt übernommen.
1. Führe den Punkt "Update vom UPSTREAM Projekt holen" aus, damit du am aktuellen Stand bist.
1. Wenn du dir nun wieder das Log ansiehst, wirst du feststellen, dass auch das upstream Repo deine Commits übernommen hat.

    ```
    $ git log
	commit 1234567890123456789012345678901234567890 (HEAD -> master,origin/master, upstream/master)
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 14:00:00 2019 +0100

		Dein 3. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 13:00:00 2019 +0100

		Dein 2. aussagekräftiger Kommentar zu deinen Änderungen.

	commit 1234567890123456789012345678901234567890
	Author: Name_Kollege <1234567-Name_Kollege@users.noreply.gitlab.com>
	Date:   Mon Jan 1 11:00:00 2019 +0100

		Aussagekräftiger Kommentar von deinem Kollegen zu seinen Änderungen.
		
	commit 1234567890123456789012345678901234567890 
	Author: Dein_Name <1234567-Dein_Name@users.noreply.gitlab.com>
	Date:   Mon Jan 1 10:00:00 2019 +0100

		Dein 1. aussagekräftiger Kommentar zu deinen Änderungen.
	
	```

# Ich bin für den closed Bereich freigeschalten. Wie bekomme ich die Inhalte?
Diesen Punkt BITTE NUR EINMAL ausführen. Damit wird dein closed Repo angelegt und heruntergeladen.

1. Geh im Explorer in den Hauptordner, in welchem sich die Lehrveranstaltung befindet, für die du freigeschaltet wurdest.
1. Öffne die git Konsole mit rechtsklick "Git Bash here"
1. führe folgende Befehle aus:

    ```
    $ git submodule init cld
    $ git submodule update cld
    ```
1. Git wird nach dem GitLab username und passwort fragen. Das muss dann hier eingegeben werden.
1. Füge das UPSTREAM Repo als Link hinzu.
    Git initialisiert die erste URL immer als origin, unabhängig davon um welches Projekt es sich handelt.
    Gib dazu folgenden Befehl ein, um die upstream URL zu sehen.
    
    ```
    $ cd ./cld
    $ git remote -v
    origin   UPSTREAM_URL_CLD (fetch)
    origin   UPSTREAM_URL_CLD (push)
    ```
    Kopiere oder tippe diese UPSTREAM_URL_CLD in folgendem Befehl ab.
    
    ```
    $ git remote add -t master upstream UPSTREAM_URL_CLD
    ```
1. Wenn du nun dir die remote Repos anzeigen lässt, sollte die UPSTREAM_URL_CLD vier mal vorkommen.

    ```
    $ git remote -v
    origin   UPSTREAM_URL_CLD (fetch)
    origin   UPSTREAM_URL_CLD (push)
    upstream UPSTREAM_URL_CLD (fetch)
    upstream UPSTREAM_URL_CLD (push)
    ```
1. Wechsle auf den aktuellen master Branch

	```
	$ git checkout master
	```
1. Erstelle deinen eigenen Branch
	
	```
	$ git branch dein_gitlab_benutzername
	```
1. Wechsel auf deinen Branch mit folgendem Befehl

	```
	$ git checkout dein_gitlab_benutzername
	```
1. Lies das README, TODO und CONTRIBUTING im closed Bereich. Es unterscheidet sich von den opn Dokumenten.
